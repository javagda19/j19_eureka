package com.j19.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class, args);
    }

}
// git init
// git add --all
// git commit -m "Initial commit"
// git remote set-url origin ADRES_SERWERA
// git remote add origin ADRES_SERWERA
// git push -u origin master
